﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon01_controller : MonoBehaviour
{
    public float rayDistance = 5;

    public LayerMask rayCastLayer;

    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance, rayCastLayer))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);

            string layerHitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

            if (layerHitted == "Trap")
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Destroy(hit.transform.gameObject);
                }
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * rayDistance, Color.red);
        }
    }
}
