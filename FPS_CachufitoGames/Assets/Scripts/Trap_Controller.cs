﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap_Controller : MonoBehaviour
{
    private int damage = 50;
    private float force_push = 3f;
    private static Trap_Controller instance;

    public static Trap_Controller Get()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Game_Manager.Get().GetDamage(damage);
            Game_Manager.Get().GetPush(force_push);
        }
    }
}
