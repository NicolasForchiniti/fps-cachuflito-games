﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Manager : MonoBehaviour {

    private int damage;
    private int life = 100;
    private static Game_Manager instance;
    public GameObject player;

    public static Game_Manager Get()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
        
    }
    // Update is called once per frame
    private void Update()
    {
        
    }
    public void GetDamage(int damage) {
        life -= damage;
        Debug.Log(life);
    }
    public void GetPush(float force_push)
    {
        player.transform.Translate(Vector3.back * force_push);
        Debug.Log("push");
    }
}
